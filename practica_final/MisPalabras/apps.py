from django.apps import AppConfig
from xml.sax.handler import ContentHandler
from xml.sax import make_parser

class MispalabrasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'MisPalabras'

class WikiDefHandler(ContentHandler):

    def __init__(self):
        self.inContent = False
        self.content = ""

    def startElement (self, name, attrs):
        if name == 'extract':
            self.inContent = True

    def endElement (self, name):
        global definition

        if name == 'extract':
            definition = "Definicion: " + self.content + "."
        if self.inContent:
            self.inContent = False
            self.content = ""

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class WikipDef:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = WikiDefHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def definition(self):
        global definition

        return definition


class FlickrImgHandler(ContentHandler):

    def __init__(self):
        self.inEntry = False
        self.inFeed = False
        self.inContent = False
        self.content = ""
        self.link = ""

    def startElement(self, name, attrs):
        if name == 'feed':
            self.inFeed = True
        elif self.inFeed:
            if name == 'entry':
                self.inEntry = True
            elif self.inEntry:
                if attrs.get('rel') == 'enclosure':
                    self.link = attrs.get('href')

    def endElement(self, name):
        global link

        if name == 'entry':
            self.inEntry = False
            self.inFeed = False
            link = self.link

class FlickrImg:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = FlickrImgHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def download_link(self):
        global link

        return link