from django.urls import path
from . import views
from django.contrib import admin
from django.contrib.auth.views import LoginView as login
from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView


urlpatterns = [
    path('', views.index),
	path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url('pages/imagenes/favicon1.png'))),
    path('loggedIn', views.loggedIn),
    path('logout', views.logout_view),
    path('ayuda', views.ayuda),
    path('crear_user', views.crear_user),
    path('eliminar_user', views.eliminar_user),
    path('login',  login.as_view()),
    path('admin', admin.site.urls),
    path('json', views.json_page),
    path('xml', views.xml_page),
    path('info_personal', views.user_info),
    path('<str:llave>', views.word),
]
