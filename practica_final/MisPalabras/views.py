from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.db.models import OuterRef, Subquery, Count
from .models import Usuario, Pagina, Comentario, Voto, Url
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
from django.db import IntegrityError
from .apps import WikipDef, FlickrImg
from django.core.paginator import Paginator
from django.core import serializers
import json
import datetime
import requests
import math
import urllib.request
from pyrae import dle

#Formulario
formulario = """
No existe valor en la base de datos
<form action="/hello" method=POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" value="Enviar">
</form>
"""
#EECE94 COLOR NARANJA
@csrf_exempt
def index(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        data = json.dumps(data)
        value = data.split("=")
        value = value[2][:-1]
        return redirect("/" + str(value))
    elif request.method == "GET":
        #Ordenar por fecha de creacion (Nuevos primero)
        words_list = Pagina.objects.all().order_by('-date')
        length = len(words_list)
        paginator = Paginator(words_list, per_page=5)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        #page_obj = paginator.page(paginator.num_pages)

        #Ordenar por nºde likes
        palabras_votadas = Pagina.objects.all().order_by('-n_votos')

        try:
            word = words_list[0]
        except IndexError:
            word = ""

        return (render(request, 'pages/INDEX.html', {
          'user': request.user,
          'words_list': words_list,
          'length': length,
          'word': word,
          'palabras_votadas': palabras_votadas,
          'page_obj': page_obj,

         }))

@csrf_exempt
def word(request, llave):
    if request.method == "POST":
        try:
            action = request.POST['action']
            if action == "Guardar":
                if request.user.is_authenticated:
                    try:
                        user = Usuario.objects.get(id=request.user.id)
                        content = download_word(request, llave)  #Descargamos definicion
                        image, width, height, text = download_image_wiki(llave)  # Descargamos Imagen ##FALTA AÑADIR IMAGEN A CLASE
                        p = Pagina(word=llave, author=user, date=datetime.datetime.now(), content=content, image=image, width=width, height=height)
                        p.save()
                        return redirect("/" + str(llave))
                    except ObjectDoesNotExist:
                        return redirect("/" + str(llave))
                else: #Si el user no está logeado no puede crear la palabra
                    return redirect("/" + str(llave))

            elif action == "Borrar palabra":
                if request.user.is_authenticated:
                    try:
                        user = Usuario.objects.get(id=request.user.id)
                        pagina = Pagina.objects.get(word=llave)
                        if user.id == pagina.author.id:
                            pagina.delete()
                        return redirect("/" + str(llave))
                    except ObjectDoesNotExist:
                        return redirect("/" + str(llave))
                else: #Si el user no está logeado no puede crear la palabra
                    return redirect("/" + str(llave))

            elif action == "Like!":
                if request.user.is_authenticated:
                    try:
                        user = Usuario.objects.get(id=request.user.id)
                        pag = Pagina.objects.get(word=llave)
                        voto = Voto.objects.get(pagina=pag, author=user)
                        return redirect("/" + str(llave))
                    except ObjectDoesNotExist:
                        try:
                            user = Usuario.objects.get(id=request.user.id)
                            pagina = Pagina.objects.get(word=llave)
                            v = Voto(pagina=pagina, author=user)
                            v.save()
                            #Añadir voto a Pagina.n_votos
                            p = Pagina.objects.get(word=llave)
                            p.n_votos += 1
                            p.save()
                            return redirect("/" + str(llave))
                        except ObjectDoesNotExist:
                            return redirect("/" + str(llave))
                else: #Si el user no está logeado no puede puntuar
                    return redirect("/" + str(llave))

            #OPCIONAL
            elif action == "Quitar Like":
                if request.user.is_authenticated:
                        #Eliminar Voto
                        user = Usuario.objects.get(id=request.user.id)
                        pag = Pagina.objects.get(word=llave)
                        voto = Voto.objects.get(pagina=pag, author=user)
                        voto.delete()
                        #Restare voto a Pagina.n_votos
                        p = Pagina.objects.get(word=llave)
                        p.n_votos -= 1
                        p.save()
                        return redirect("/" + str(llave))
                else: #Si el user no está logeado no puede puntuar
                    return redirect("/" + str(llave))

            elif action == "rae":
                if request.user.is_authenticated:
                        #Descargamos definicion
                        pag = Pagina.objects.get(word=llave)
                        definicion = rae(llave)
                        pag.rae = definicion
                        pag.save()
                        return redirect("/" + str(llave))
                else: #Si el user no está logeado no puede puntuar
                    return redirect("/" + str(llave))

            elif action == "flickr":
                try:
                    flickr_image = download_image_flickr(llave)
                    image = Pagina.objects.get(flickr_image=flickr_image)
                    return redirect("/" + str(llave))
                except ObjectDoesNotExist:
                    flickr_image = download_image_flickr(llave)
                    pagina = Pagina.objects.get(word=llave)
                    pagina.flickr_image = flickr_image
                    pagina.save()
                    return redirect("/" + str(llave))

            elif action == "Meme 1":
                 meme_image = download_meme(llave, 1)
                 pagina = Pagina.objects.get(word=llave)
                 pagina.meme = meme_image
                 pagina.save()
                 return redirect("/" + str(llave))

            elif action == "Meme 2":
                meme_image = download_meme(llave, 2)
                pagina = Pagina.objects.get(word=llave)
                pagina.meme = meme_image
                pagina.save()
                return redirect("/" + str(llave))

            elif action == "Meme 3":
                meme_image = download_meme(llave, 3)
                pagina = Pagina.objects.get(word=llave)
                pagina.meme = meme_image
                pagina.save()
                return redirect("/" + str(llave))

        except MultiValueDictKeyError:
            pass

        #POSTs mediante caja de texto
        try:
            action = request.POST['Comentario']
            if request.user.is_authenticated:
                try:
                    user = Usuario.objects.get(id=request.user.id)
                    pag = Pagina.objects.get(word=llave)
                    comentario = Comentario.objects.get(page=pag, author=user)
                    return redirect("/" + str(llave))
                except ObjectDoesNotExist:
                    data = request.POST.get("Comentario")
                    user = Usuario.objects.get(id=request.user.id)
                    pag = Pagina.objects.get(word=llave)
                    c = Comentario(page=pag, author=user, comment=data, date=datetime.datetime.now())
                    c.save()
                    return redirect("/" + str(llave))
            else: #Si el user no está logeado no puede comentar
                return redirect("/" + str(llave))
        except MultiValueDictKeyError:
            pass

        try:
            action = request.POST['Url']
            if request.user.is_authenticated:
                try:
                    user = Usuario.objects.get(id=request.user.id)
                    pag = Pagina.objects.get(word=llave)
                    url = Url.objects.get(page=pag, author=user)
                    return redirect("/" + str(llave))
                except ObjectDoesNotExist:
                    data = request.POST.get("Url")
                    user = Usuario.objects.get(id=request.user.id)
                    pag = Pagina.objects.get(word=llave)
                    c = Url(page=pag, author=user, comment=data, date=datetime.datetime.now())
                    c.save()
                    return redirect("/" + str(llave))
            else: #Si el user no está logeado no puede comentar
                return redirect("/" + str(llave))
        except MultiValueDictKeyError:
            pass

    #Recibimos un GET
    elif request.method == "GET":
        content = download_word(request, llave) #Descargamos definicion
        image, width, height, text = download_image_wiki(llave) #Descargamos Imagen

        #Comprobamos si la página está guardada
        try:
            w = Pagina.objects.get(word=llave)
            existe = True
        except ObjectDoesNotExist:
            existe = False

        # Comprobamos si tiene votos
        try:
            word = Pagina.objects.get(word=llave)
            votos = Voto.objects.filter(pagina=word)
            n_votos = len(votos)
        except ObjectDoesNotExist:
            n_votos = 0

        #Comprobar si usuario ha dado like previamente
        try:
            user = Usuario.objects.get(id=request.user.id)
            word = Pagina.objects.get(word=llave)
            like = Voto.objects.get(pagina=word, author=user)
            like_btn = True
        except ObjectDoesNotExist:
            like_btn = False

        #Lista de comentarios de la pagina
        try:
            pagina = Pagina.objects.get(word=llave)
            list_comentarios = Comentario.objects.filter(page=pagina)
        except ObjectDoesNotExist:
            list_comentarios = "No hay comentarios de momento"

        #Elegimos la primera palabra guardada para display inferior y long palabras almacenadas
        words_list = Pagina.objects.all().order_by('-date')
        length = len(words_list)
        try:
            palabra = words_list[0]
        except IndexError:
            palabra = ""

        #Obtenemos vars de FlickImage
        try:
            flickr_image = download_image_flickr(llave)
            flickr = Pagina.objects.get(flickr_image=flickr_image)
            flickr_btn = True
        except ObjectDoesNotExist:
            flickr_btn = False

        try:
            word = Pagina.objects.get(word=llave)
        except ObjectDoesNotExist:
            word = None

        #Lista de URLS
        try:
            pagina = Pagina.objects.get(word=llave)
            list_urls = Url.objects.filter(page=pagina)
        except ObjectDoesNotExist:
            list_urls = "No hay URLS de momento"

        #Comprobar si existe RAE
        try:
            try:
                rae_content = rae(llave)
            except KeyError:
                rae_content = "No hay definicion de la RAE para esta palabra"
                content = Pagina.objects.get(word=llave)
                content.rae = rae_content
            pagina = Pagina.objects.get(word=llave, rae=rae_content)
            rae_stored = True
        except ObjectDoesNotExist:
            rae_stored = False

        #Comprobar si existe Meme
        try:
            meme = Pagina.objects.get(meme='static/pages/imagenes/' + llave + '.jpg')
            meme_existe = True
        except ObjectDoesNotExist:
            meme_existe = False




        return (render(request, 'pages/word.html', {
        'user': request.user,
        'pagina': word,
        'word': llave,
        'existe': existe,
        'content': content,
        'image': image,
        'width': width,
        'height': height,
        'text': text,
        'likes': n_votos,
        'length': length,
        'palabra': palabra,
        'like_btn': like_btn,
        'list_comentarios': list_comentarios,
        'flickr_btn': flickr_btn,
        'list_urls': list_urls,
        'rae_stored': rae_stored,
        'meme_existe': meme_existe,
        }))

def download_word(request, word):
    try:
        url = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles='+ word + \
          '&prop=extracts&exintro&explaintext'
        xmlStream = urllib.request.urlopen(url)
        content = WikipDef(xmlStream).definition()
    except NameError:
        content = 'No existe definición en Wikipedia de esta entrada'
    return content

def download_image_wiki(word):
    url = 'https://es.wikipedia.org/w/api.php?action=query&titles=' + word + \
          '&prop=pageimages&format=json&pithumbsize=200'
    with urllib.request.urlopen(url) as json_doc:
        json_str = json_doc.read().decode(encoding="ISO-8859-1")
        archivo = json.loads(json_str)
    image = archivo['query']['pages']
    id = list(image.keys())
    try:
        image = image[str(id[0])]['thumbnail']
        source = image['source']
        height = image['height']
        width = image['width']
        answer = 'Imagen que ofrece wikipedia de ' + word
    except:
        source = ''
        height = ''
        width = ''
        answer = 'Esta palabra no tiene imagen en Wikipedia en español.'
    return source, height, width, answer

def download_image_flickr(word):
    url = 'https://www.flickr.com/services/feeds/photos_public.gne?tags=' + word
    xmlStream = urllib.request.urlopen(url)
    image = FlickrImg(xmlStream).download_link()
    return image

def rae(word):
    res = dle.search_by_word(word=word)
    palabra = res.to_dict()
    try:
        palabra = palabra['articles'][0]['definitions'][0]['sentence']['text']
    except KeyError:
        palabra = "No hay definicion de la RAE para esta palabra"
    return palabra

def download_meme(word, option):
    f = open('MisPalabras/static/pages/imagenes/' + word + '.jpg', 'wb')
    if option == 1:
        response = requests.get('https://apimeme.com/meme?meme=Arthur-Fist&top=¿' + word +'?&bottom=Dont think so')
    elif option == 2:
        response = requests.get('https://apimeme.com/meme?meme=Ancient-Aliens&top=¿' + word + '?&bottom=ALIENS')
    elif option == 3:
        response = requests.get('https://apimeme.com/meme?meme=10-Guy&top=You say ' + word + '&bottom=Bedtime?')
    else:
        response = requests.get('https://apimeme.com/meme?meme=10-Guy&top=You say ' + word + '&bottom=Bedtime?')
    f.write(response.content)
    f.close()
    link = 'static/pages/imagenes/' + word + '.jpg'

    return link

def xml_page(request):
    data = serializers.serialize("xml", Pagina.objects.all())
    list_words = Pagina.objects.all()
    return (render(request, 'pages/xml.html', {
        'list_words': list_words,
        'data': data
    }))

def json_page(request):
    data = serializers.serialize("json", Pagina.objects.all())
    return (render(request, 'pages/json.html', {
        'data': data
    }))

@csrf_exempt
def crear_user(request):
    if request.method == "POST":
        try:
            name = request.POST['username']
            password = request.POST['password']
            #Crear usuario
            User.objects.create_user(username=name, password=password)
            return redirect("/")
        except MultiValueDictKeyError:
            return redirect("/crear_user")

    elif request.method == "GET":
        return (render(request, 'pages/crear_user.html', {
        }))

@csrf_exempt
def eliminar_user(request):
    u = User.objects.get(id=request.user.id)
    u.delete()
    u = Usuario.objects.get(id=request.user.id)
    u.delete()
    return redirect("/")

@csrf_exempt
def loggedIn(request):
    if request.user.is_authenticated:
        try: #Comprobamos si ya existe ese usuario
            u = Usuario.objects.get(id=request.user.id)
        except ObjectDoesNotExist: #Sino existe lo crea y guarda
            u = Usuario(id=request.user.id, name=request.user.username)
            u.save()
    return redirect("/")

@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect("/")

def ayuda(request):
    content_list = request.user
    template = loader.get_template('pages/AYUDA.html')
    context = {
        'content_list': content_list
    }
    return HttpResponse(template.render(context, request))

def user_info(request):  ###COMPLETAR####
    #Conseguir todos los datos del usuario
    user = Usuario.objects.get(id=request.user.id)
    paginas_creadas = Pagina.objects.filter(author=user)
    paginas_creadas = paginas_creadas.order_by('-date')

    comentarios_creados = Comentario.objects.filter(author=user)
    comentarios_creados = comentarios_creados.order_by('-date')

    urls_creados = Url.objects.filter(author=user)
    urls_creados = urls_creados.order_by('-date')

    votos_creados = Voto.objects.filter(author=user)

    #Cargamos plantilla de su información
    return (render(request, 'pages/info_personal.html', {
        'user': request.user,
        'paginas_creadas': paginas_creadas,
        'Comentarios_creados': comentarios_creados,
        'Votos_creados': votos_creados,
        'urls_creados': urls_creados,

        }))
