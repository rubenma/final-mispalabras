from django.contrib import admin
from .models import Usuario, Pagina, Comentario, Voto

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Comentario)
admin.site.register(Pagina)
admin.site.register(Voto)
