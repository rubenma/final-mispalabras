# Generated by Django 4.0.3 on 2022-06-25 01:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MisPalabras', '0005_alter_pagina_n_votos'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pagina',
            name='n_votos',
            field=models.IntegerField(default=0),
        ),
    ]
