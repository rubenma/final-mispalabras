# Generated by Django 4.0.3 on 2022-06-27 22:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MisPalabras', '0013_pagina_meme'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pagina',
            name='meme',
            field=models.CharField(default='', max_length=1024, null=True),
        ),
    ]
