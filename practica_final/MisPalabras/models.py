from django.db import models

# Create your models here.
class Usuario(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name

class Pagina(models.Model):
    word = models.CharField(max_length=128)
    content = models.CharField(max_length=1024)
    rae = models.CharField(max_length=1024, default='')
    flickr_image = models.CharField(max_length=1024, default='')
    meme = models.CharField(max_length=1024, default='')
    image = models.CharField(max_length=1024, default='')
    width = models.CharField(max_length=1024, default='')
    height = models.CharField(max_length=1024, default='')
    author = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE)
    date = models.DateTimeField(null=True, blank=True)
    n_votos = models.IntegerField(default=0)
    def __str__(self):
        return self.word

class Voto(models.Model):
    pagina = models.ForeignKey(Pagina, null=True, blank=True, on_delete=models.CASCADE)
    author = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE)
    def __str__(self):
        return self.pagina.word

class Comentario(models.Model):
    page = models.ForeignKey(Pagina, null=True, blank=True, on_delete=models.CASCADE)
    author = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE)
    comment = models.TextField(default='')
    date = models.DateTimeField(null=True, blank=True)
    def __str__(self):
        return self.page.word

class Url(models.Model):
    page = models.ForeignKey(Pagina, null=True, blank=True, on_delete=models.CASCADE)
    author = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE)
    comment = models.TextField(default='')
    date = models.DateTimeField(null=True, blank=True)
    def __str__(self):
        return self.page.word



