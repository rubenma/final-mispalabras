import unittest
from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User
from .models import Usuario, Pagina, Comentario, Voto, Url

# Create your tests here.
class GET_tests(TestCase):

    def tests(self):
        #TEST /
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<header><h2>Mis palabras</h2></header>', content)

        #TEST /AYUDA
        c = Client()
        response = c.get('/ayuda')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h3>Autor: Rubén Martín Martín </h3>', content)

        # TEST /ITEM
        c = Client()
        response = c.get('/ojo')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h4>Definición Wikipedia Español:</h4>', content)

        # TEST /JSON
        c = Client()
        response = c.get('/json')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h4>Contenido JSON:</h4>', content)

        # TEST /XML
        c = Client()
        response = c.get('/xml')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h4>Contenido XML:</h4>', content)

        # TEST /LOGIN
        c = Client()
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Username', content)

        # TEST /INFO_PERSONAL
        user = User.objects.create_user(username='test', password='test2000')
        user.save()
        test = Usuario(id=user.id, name=user.username)
        test.save()
        login = self.client.login(username='test', password='test2000')
        response = self.client.get('/info_personal')
        self.assertEqual(response.status_code, 200)

        # TEST /logout
        response = c.get('/logout')
        self.assertEqual(response.status_code, 302)







