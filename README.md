# Final MisPalabras

# Entrega practica

## Datos

* Nombre: Rubén Martín Martín
* Titulación: Sistemas de Telecomunicación
* Video básico (url): https://youtu.be/YWi6BXoEqng 
* Video parte opcional (url): https://youtu.be/ZwdH2zl17vk 
* Despliegue (url): http://rubenmarmar.pythonanywhere.com/

## Cuenta Admin Site

* admin/2000admin2000

## Cuentas usuarios

* ruben/ruben2000
* juan/juan2000
* sara/sara2000
* margarita/marga2000
* 
## Lista partes opcionales

* Quitar like:
* Es posible quitar un voto a una palabra que haya votado el mismo usuario con anterioridad.
* Eliminar cuentas de usuario:
* Un usuario puede eliminar su cuenta y todos los datos relacionados con la misma.
* Nombre parte: Favicon.io
* La aplicación tiene un favicon.io incluido, siendo una imagen de una letra "M" con fondo verde.
* Borrar Palabras almacenadas
* Un usuario que haya creado una palabra puede borrarla, solo puede borrar esa palabra el usuario que la haya creado.
